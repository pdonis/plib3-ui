plib3.ui To Do List
===================

Bug Fixes
---------

- Fix focus in/out support for numeric edit boxes

- Progress dialog in wx does not abort when dialog
  window close button is used.

Planned Features
----------------

- Add more widgets (possibles are date and time edit,
  color picker, font picker, clock, slider).

- Add support for expanded keyboard signals.

- Add support for mouse wheel signals.

- Add support for mouse drag and drop.
